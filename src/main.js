import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { ModalPlugin } from 'bootstrap-vue'


Vue.use(ModalPlugin);


//styles
import './assets/styles/style.scss'

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
