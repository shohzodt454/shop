import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import axios from "axios";

const initialState = () => ({
  userData: null,
  posts: [],
  filteredPosts: [],
  product: null,
  cart: []
})

Vue.use(Vuex);

export default new Vuex.Store({
  state: initialState(),
  mutations: {
    ADD_TOCART(state, payload) {
      let productInCart = state.cart.find(item => {
        return item.id === payload.id;
      });

      if (productInCart) {
        productInCart.count += 1;
        return;
      }

      state.cart.push(payload)
    },
    INCREMENT(state, item) {
      state.cart.forEach(product => {
        if (product.id === item.id) {
          product.count += 1;
        }
        return product;
      });
    },
    DECREMENT(state, item) {
      state.cart.forEach(product => {
        if (product.id === item.id && product.count > 1) {
          product.count -= 1;
        }
        else if (product.id === item.id && product.count == 1) {
          const index = state.cart.indexOf(product)
          if (index > -1) {
            state.cart.splice(index, 1)
          }
        }
        return product;
      });
    },

    SET_USER(state, payload) {
      state.product = payload
    },
    SET_POSTS(state, payload) {
      state.posts = payload
      state.filteredPosts = payload
    },
    SET_USER(state, payload) {
      state.userData = payload
    },
    SET_SEARCHTEXT(state, payload) {
      if (!payload) {
        return state.posts
      };

      state.filteredPosts = state.posts.filter((item) => {
        return item.title.startsWith(payload);
      })
    },
    LOG_OUT(state) {
      Object.assign(state, initialState())
    }
  },
  actions: {
    setUserData({ commit }, { username, password }) {
      return new Promise((resolve) => {
        const userData = {
          username,
          password
        }
        commit('SET_USER', userData)
        resolve(userData)
      })
    },
    async fetchPost(ctx, id) {
      return new Promise((resolve, reject) => {
        axios.get(`https://jsonplaceholder.typicode.com/photos/${id}`)
          .then((response) => resolve(response))
          .catch((err) => reject(err))
      })
    },
    async fetchPosts({ commit }) {
      try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/photos');

        commit('SET_POSTS', response.data)
      }
      catch (e) {
        console.log(e)
      }
    }

  },
  getters: {
    isAuthenticated(state) {
      return state.userData
    }
  },
  plugins: [createPersistedState({
    key: "persistedStates",
    paths: ["userData", "cart"]
  })],
});
