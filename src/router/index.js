import Vue from "vue";
import VueRouter from "vue-router";
import store from '@/store'
import products from './routes/products'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    meta: { requiresAuth: true },
    component: () => import("@/views/Home.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/Login.vue"),
  },
  {
    path: "/cart",
    name: "Cart",
    meta: { requiresAuth: true },
    component: () => import("@/views/Cart.vue"),
  },
  ...products
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const isAuthenticated = store.getters['isAuthenticated']
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!isAuthenticated) {
      next({ path: "/login" });
      return
    } next();
    return
  } next();
});


export default router;
