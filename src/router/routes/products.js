export default [
    {
        path: '/products',
        name: 'products',
        meta: { requiresAuth: true },
        component: () => import('@/views/products/products-list/ProductsList.vue'),
    },
    {
        path: '/products/:id',
        name: 'products-view',
        meta: { requiresAuth: true },
        component: () => import('@/views/products/products-view/ProductsView.vue')
    },

]
